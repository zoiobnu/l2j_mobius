/*
 * This file is part of the L2J Mobius project.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jmobius.gameserver.model.actor.instance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

import com.l2jmobius.Config;
import com.l2jmobius.commons.concurrent.ThreadPool;
import com.l2jmobius.gameserver.ai.CtrlIntention;
import com.l2jmobius.gameserver.ai.L2CharacterAI;
import com.l2jmobius.gameserver.ai.L2DoorAI;
import com.l2jmobius.gameserver.instancemanager.CastleManager;
import com.l2jmobius.gameserver.instancemanager.FortManager;
import com.l2jmobius.gameserver.model.L2Clan;
import com.l2jmobius.gameserver.model.L2Object;
import com.l2jmobius.gameserver.model.L2Skill;
import com.l2jmobius.gameserver.model.L2Territory;
import com.l2jmobius.gameserver.model.actor.L2Character;
import com.l2jmobius.gameserver.model.actor.L2Playable;
import com.l2jmobius.gameserver.model.actor.knownlist.DoorKnownList;
import com.l2jmobius.gameserver.model.actor.position.Location;
import com.l2jmobius.gameserver.model.actor.stat.DoorStat;
import com.l2jmobius.gameserver.model.actor.status.DoorStatus;
import com.l2jmobius.gameserver.model.entity.ClanHall;
import com.l2jmobius.gameserver.model.entity.siege.Castle;
import com.l2jmobius.gameserver.model.entity.siege.Fort;
import com.l2jmobius.gameserver.model.entity.siege.clanhalls.DevastatedCastle;
import com.l2jmobius.gameserver.network.L2GameClient;
import com.l2jmobius.gameserver.network.serverpackets.ActionFailed;
import com.l2jmobius.gameserver.network.serverpackets.ConfirmDlg;
import com.l2jmobius.gameserver.network.serverpackets.DoorStatusUpdate;
import com.l2jmobius.gameserver.network.serverpackets.MyTargetSelected;
import com.l2jmobius.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jmobius.gameserver.network.serverpackets.SystemMessage;
import com.l2jmobius.gameserver.network.serverpackets.ValidateLocation;
import com.l2jmobius.gameserver.templates.chars.L2CharTemplate;
import com.l2jmobius.gameserver.templates.item.L2Weapon;

public class L2DoorInstance extends L2Character
{
	protected static final Logger LOGGER = Logger.getLogger(L2DoorInstance.class.getName());
	
	private int _castleIndex = -2;
	private int _mapRegion = -1;
	private int _fortIndex = -2;
	private int _rangeXMin = 0;
	private int _rangeYMin = 0;
	private int _rangeZMin = 0;
	private int _rangeXMax = 0;
	private int _rangeYMax = 0;
	private int _rangeZMax = 0;
	private int _A = 0;
	private int _B = 0;
	private int _C = 0;
	private int _D = 0;
	protected final int _doorId;
	protected final String _name;
	boolean _open;
	private final boolean _unlockable;
	private ClanHall _clanHall;
	protected int _autoActionDelay = -1;
	private ScheduledFuture<?> _autoActionTask;
	public final L2Territory pos;
	
	/**
	 * This class may be created only by L2Character and only for AI.
	 */
	public class AIAccessor extends L2Character.AIAccessor
	{
		protected AIAccessor()
		{
		}
		
		@Override
		public L2DoorInstance getActor()
		{
			return L2DoorInstance.this;
		}
		
		@Override
		public void moveTo(int x, int y, int z, int offset)
		{
		}
		
		@Override
		public void moveTo(int x, int y, int z)
		{
		}
		
		@Override
		public void stopMove(Location pos)
		{
		}
		
		@Override
		public void doAttack(L2Character target)
		{
		}
		
		@Override
		public void doCast(L2Skill skill)
		{
		}
	}
	
	@Override
	public L2CharacterAI getAI()
	{
		if (_ai == null)
		{
			synchronized (this)
			{
				if (_ai == null)
				{
					_ai = new L2DoorAI(new AIAccessor());
				}
			}
		}
		return _ai;
	}
	
	@Override
	public boolean hasAI()
	{
		return _ai != null;
	}
	
	class CloseTask implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				onClose();
			}
			catch (Throwable e)
			{
				LOGGER.warning(e.getMessage());
			}
		}
	}
	
	/**
	 * Manages the auto open and closing of a door.
	 */
	class AutoOpenClose implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				String doorAction;
				
				if (!_open)
				{
					doorAction = "opened";
					openMe();
				}
				else
				{
					doorAction = "closed";
					closeMe();
				}
				
				if (Config.DEBUG)
				{
					LOGGER.info("Auto " + doorAction + " door ID " + _doorId + " (" + _name + ") for " + (_autoActionDelay / 60000) + " minute(s).");
				}
			}
			catch (Exception e)
			{
				LOGGER.warning("Could not auto open/close door ID " + _doorId + " (" + _name + ")");
			}
		}
	}
	
	/**
	 * Instantiates a new l2 door instance.
	 * @param objectId the object id
	 * @param template the template
	 * @param doorId the door id
	 * @param name the name
	 * @param unlockable the unlockable
	 */
	public L2DoorInstance(int objectId, L2CharTemplate template, int doorId, String name, boolean unlockable)
	{
		super(objectId, template);
		getKnownList(); // init knownlist
		getStat(); // init stats
		getStatus(); // init status
		_doorId = doorId;
		_name = name;
		_unlockable = unlockable;
		pos = new L2Territory();
	}
	
	@Override
	public final DoorKnownList getKnownList()
	{
		if ((super.getKnownList() == null) || !(super.getKnownList() instanceof DoorKnownList))
		{
			setKnownList(new DoorKnownList(this));
		}
		
		return (DoorKnownList) super.getKnownList();
	}
	
	@Override
	public final DoorStat getStat()
	{
		if ((super.getStat() == null) || !(super.getStat() instanceof DoorStat))
		{
			setStat(new DoorStat(this));
		}
		
		return (DoorStat) super.getStat();
	}
	
	@Override
	public final DoorStatus getStatus()
	{
		if ((super.getStatus() == null) || !(super.getStatus() instanceof DoorStatus))
		{
			setStatus(new DoorStatus(this));
		}
		
		return (DoorStatus) super.getStatus();
	}
	
	/**
	 * Checks if is unlockable.
	 * @return true, if is unlockable
	 */
	public final boolean isUnlockable()
	{
		return _unlockable;
	}
	
	@Override
	public final int getLevel()
	{
		return 1;
	}
	
	/**
	 * Gets the door id.
	 * @return Returns the doorId.
	 */
	public int getDoorId()
	{
		return _doorId;
	}
	
	/**
	 * Gets the open.
	 * @return Returns the open.
	 */
	public boolean getOpen()
	{
		return _open;
	}
	
	/**
	 * Sets the open.
	 * @param open The open to set.
	 */
	public void setOpen(boolean open)
	{
		_open = open;
	}
	
	/**
	 * Sets the delay in milliseconds for automatic opening/closing of this door instance. <BR>
	 * <B>Note:</B> A value of -1 cancels the auto open/close task.
	 * @param actionDelay the new auto action delay
	 */
	public void setAutoActionDelay(int actionDelay)
	{
		if (_autoActionDelay == actionDelay)
		{
			return;
		}
		
		if (actionDelay > -1)
		{
			AutoOpenClose ao = new AutoOpenClose();
			ThreadPool.scheduleAtFixedRate(ao, actionDelay, actionDelay);
		}
		else if (_autoActionTask != null)
		{
			_autoActionTask.cancel(false);
		}
		
		_autoActionDelay = actionDelay;
	}
	
	/**
	 * Gets the damage.
	 * @return the damage
	 */
	public int getDamage()
	{
		final int dmg = 6 - (int) Math.ceil((getCurrentHp() / getMaxHp()) * 6);
		if (dmg > 6)
		{
			return 6;
		}
		if (dmg < 0)
		{
			return 0;
		}
		return dmg;
	}
	
	/**
	 * Gets the castle.
	 * @return the castle
	 */
	public final Castle getCastle()
	{
		if (_castleIndex < 0)
		{
			_castleIndex = CastleManager.getInstance().getCastleIndex(this);
		}
		
		if (_castleIndex < 0)
		{
			return null;
		}
		
		return CastleManager.getInstance().getCastles().get(_castleIndex);
	}
	
	/**
	 * Gets the fort.
	 * @return the fort
	 */
	public final Fort getFort()
	{
		if (_fortIndex < 0)
		{
			_fortIndex = FortManager.getInstance().getFortIndex(this);
		}
		
		if (_fortIndex < 0)
		{
			return null;
		}
		
		return FortManager.getInstance().getForts().get(_fortIndex);
	}
	
	/**
	 * Sets the clan hall.
	 * @param clanhall the new clan hall
	 */
	public void setClanHall(ClanHall clanhall)
	{
		_clanHall = clanhall;
	}
	
	/**
	 * Gets the clan hall.
	 * @return the clan hall
	 */
	public ClanHall getClanHall()
	{
		return _clanHall;
	}
	
	/**
	 * Checks if is enemy of.
	 * @param cha the cha
	 * @return true, if is enemy of
	 */
	public boolean isEnemyOf(L2Character cha)
	{
		return true;
	}
	
	@Override
	public boolean isAutoAttackable(L2Character attacker)
	{
		if (_unlockable)
		{
			return true;
		}
		
		// Doors can't be attacked by NPCs
		if ((attacker == null) || !(attacker instanceof L2Playable))
		{
			return false;
		}
		
		// Attackable during siege by attacker only
		L2PcInstance player = null;
		if (attacker instanceof L2PcInstance)
		{
			player = (L2PcInstance) attacker;
		}
		else if (attacker instanceof L2SummonInstance)
		{
			player = ((L2SummonInstance) attacker).getOwner();
		}
		else if (attacker instanceof L2PetInstance)
		{
			player = ((L2PetInstance) attacker).getOwner();
		}
		
		if (player == null)
		{
			return false;
		}
		
		final L2Clan clan = player.getClan();
		final boolean isCastle = (getCastle() != null) && (getCastle().getCastleId() > 0) && getCastle().getSiege().getIsInProgress() && getCastle().getSiege().checkIsAttacker(clan);
		final boolean isFort = (getFort() != null) && (getFort().getFortId() > 0) && getFort().getSiege().getIsInProgress() && getFort().getSiege().checkIsAttacker(clan);
		if (isFort)
		{
			if ((clan != null) && (clan == getFort().getOwnerClan()))
			{
				return false;
			}
		}
		else if (isCastle)
		{
			if ((clan != null) && (clan.getClanId() == getCastle().getOwnerId()))
			{
				return false;
			}
		}
		return isCastle || isFort || DevastatedCastle.getInstance().getIsInProgress();
	}
	
	/**
	 * Checks if is attackable.
	 * @param attacker the attacker
	 * @return true, if is attackable
	 */
	public boolean isAttackable(L2Character attacker)
	{
		return isAutoAttackable(attacker);
	}
	
	@Override
	public void updateAbnormalEffect()
	{
	}
	
	/**
	 * Gets the distance to watch object.
	 * @param object the object
	 * @return the distance to watch object
	 */
	public int getDistanceToWatchObject(L2Object object)
	{
		if (!(object instanceof L2PcInstance))
		{
			return 0;
		}
		return 2000;
	}
	
	/**
	 * Return the distance after which the object must be remove from _knownObject according to the type of the object.<BR>
	 * <BR>
	 * <B><U> Values </U> :</B><BR>
	 * <BR>
	 * <li>object is a L2PcInstance : 4000</li>
	 * <li>object is not a L2PcInstance : 0</li><BR>
	 * <BR>
	 * @param object the object
	 * @return the distance to forget object
	 */
	public int getDistanceToForgetObject(L2Object object)
	{
		if (!(object instanceof L2PcInstance))
		{
			return 0;
		}
		
		return 4000;
	}
	
	/**
	 * Return null.<BR>
	 * <BR>
	 * @return the active weapon instance
	 */
	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		return null;
	}
	
	@Override
	public L2Weapon getActiveWeaponItem()
	{
		return null;
	}
	
	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		return null;
	}
	
	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		return null;
	}
	
	@Override
	public void onAction(L2PcInstance player)
	{
		if (player == null)
		{
			return;
		}
		
		if (Config.DEBUG)
		{
			LOGGER.info("player " + player.getObjectId());
			LOGGER.info("Door " + getObjectId());
			LOGGER.info("player clan " + player.getClan());
			if (player.getClan() != null)
			{
				LOGGER.info("player clanid " + player.getClanId());
				LOGGER.info("player clanleaderid " + player.getClan().getLeaderId());
			}
			LOGGER.info("clanhall " + _clanHall);
			if (_clanHall != null)
			{
				LOGGER.info("clanhallID " + _clanHall.getId());
				LOGGER.info("clanhallOwner " + _clanHall.getOwnerId());
				for (L2DoorInstance door : _clanHall.getDoors())
				{
					LOGGER.info("clanhallDoor " + door.getObjectId());
				}
			}
		}
		
		// Check if the L2PcInstance already target the L2NpcInstance
		if (this != player.getTarget())
		{
			// Set the target of the L2PcInstance player
			player.setTarget(this);
			
			// Send a Server->Client packet MyTargetSelected to the L2PcInstance player
			MyTargetSelected my = new MyTargetSelected(getObjectId(), 0);
			player.sendPacket(my);
			
			DoorStatusUpdate su = new DoorStatusUpdate(this);
			player.sendPacket(su);
			
			// Send a Server->Client packet ValidateLocation to correct the L2NpcInstance position and heading on the client
			player.sendPacket(new ValidateLocation(this));
		}
		else if (isAutoAttackable(player))
		{
			if (Math.abs(player.getZ() - getZ()) < 400) // this max heigth difference might need some tweaking
			{
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, this);
			}
		}
		else if ((player.getClan() != null) && (_clanHall != null) && (player.getClanId() == _clanHall.getOwnerId()))
		{
			if (!isInsideRadius(player, L2NpcInstance.INTERACTION_DISTANCE, false, false))
			{
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
			}
			else
			{
				// Like L2OFF Clanhall's doors get request to be closed/opened
				player.gatesRequest(this);
				if (!_open)
				{
					player.sendPacket(new ConfirmDlg(1140));
				}
				else
				{
					player.sendPacket(new ConfirmDlg(1141));
				}
			}
		}
		// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}
	
	@Override
	public void onActionShift(L2GameClient client)
	{
		L2PcInstance player = client.getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (Config.DEBUG)
		{
			LOGGER.info("player " + player.getObjectId());
			LOGGER.info("Door " + getObjectId());
			LOGGER.info("player clan " + player.getClan());
			if (player.getClan() != null)
			{
				LOGGER.info("player clanid " + player.getClanId());
				LOGGER.info("player clanleaderid " + player.getClan().getLeaderId());
			}
			LOGGER.info("clanhall " + _clanHall);
			if (_clanHall != null)
			{
				LOGGER.info("clanhallID " + _clanHall.getId());
				LOGGER.info("clanhallOwner " + _clanHall.getOwnerId());
				for (L2DoorInstance door : _clanHall.getDoors())
				{
					LOGGER.info("clanhallDoor " + door.getObjectId());
				}
			}
		}
		
		if (player.getAccessLevel().isGm())
		{
			player.setTarget(this);
			MyTargetSelected my = new MyTargetSelected(getObjectId(), player.getLevel());
			player.sendPacket(my);
			
			if (isAutoAttackable(player))
			{
				DoorStatusUpdate su = new DoorStatusUpdate(this);
				player.sendPacket(su);
			}
			
			NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
			StringBuilder html1 = new StringBuilder("<html><body><table border=0>");
			html1.append("<tr><td>S.Y.L. Says:</td></tr>");
			html1.append("<tr><td>Current HP  " + getCurrentHp() + "</td></tr>");
			html1.append("<tr><td>Max HP       " + getMaxHp() + "</td></tr>");
			
			html1.append("<tr><td>Object ID: " + getObjectId() + "</td></tr>");
			html1.append("<tr><td>Door ID: " + _doorId + "</td></tr>");
			html1.append("<tr><td><br></td></tr>");
			
			html1.append("<tr><td>Class: " + getClass().getName() + "</td></tr>");
			html1.append("<tr><td><br></td></tr>");
			html1.append("</table>");
			
			html1.append("<table><tr>");
			html1.append("<td><button value=\"Open\" action=\"bypass -h admin_open " + _doorId + "\" width=40 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td>");
			html1.append("<td><button value=\"Close\" action=\"bypass -h admin_close " + _doorId + "\" width=40 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td>");
			html1.append("<td><button value=\"Kill\" action=\"bypass -h admin_kill\" width=40 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td>");
			html1.append("<td><button value=\"Delete\" action=\"bypass -h admin_delete\" width=40 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td>");
			html1.append("</tr></table></body></html>");
			
			html.setHtml(html1.toString());
			player.sendPacket(html);
		}
		else
		{
			// ATTACK the mob without moving?
			player.setTarget(this);
			MyTargetSelected my = new MyTargetSelected(getObjectId(), player.getLevel());
			player.sendPacket(my);
			
			if (isAutoAttackable(player))
			{
				DoorStatusUpdate su = new DoorStatusUpdate(this);
				player.sendPacket(su);
			}
			
			final NpcHtmlMessage reply = new NpcHtmlMessage(5);
			final StringBuilder replyMsg = new StringBuilder("<html><body>You cannot use this action.");
			replyMsg.append("</body></html>");
			reply.setHtml(replyMsg.toString());
			player.sendPacket(reply);
			player.getClient().sendPacket(ActionFailed.STATIC_PACKET);
			return;
		}
		
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}
	
	@Override
	public void broadcastStatusUpdate()
	{
		final Collection<L2PcInstance> knownPlayers = getKnownList().getKnownPlayers().values();
		
		if ((knownPlayers == null) || knownPlayers.isEmpty())
		{
			return;
		}
		
		final DoorStatusUpdate su = new DoorStatusUpdate(this);
		
		for (L2PcInstance player : knownPlayers)
		{
			player.sendPacket(su);
		}
	}
	
	public void onOpen()
	{
		ThreadPool.schedule(new CloseTask(), 60000);
	}
	
	public void onClose()
	{
		closeMe();
	}
	
	public final void closeMe()
	{
		synchronized (this)
		{
			if (!_open)
			{
				return;
			}
			
			setOpen(false);
		}
		
		broadcastStatusUpdate();
	}
	
	public final void openMe()
	{
		synchronized (this)
		{
			if (_open)
			{
				return;
			}
			setOpen(true);
		}
		
		broadcastStatusUpdate();
	}
	
	@Override
	public String toString()
	{
		return "door " + _doorId;
	}
	
	/**
	 * Gets the door name.
	 * @return the door name
	 */
	public String getDoorName()
	{
		return _name;
	}
	
	/**
	 * Gets the x min.
	 * @return the x min
	 */
	public int getXMin()
	{
		return _rangeXMin;
	}
	
	/**
	 * Gets the y min.
	 * @return the y min
	 */
	public int getYMin()
	{
		return _rangeYMin;
	}
	
	/**
	 * Gets the z min.
	 * @return the z min
	 */
	public int getZMin()
	{
		return _rangeZMin;
	}
	
	/**
	 * Gets the x max.
	 * @return the x max
	 */
	public int getXMax()
	{
		return _rangeXMax;
	}
	
	/**
	 * Gets the y max.
	 * @return the y max
	 */
	public int getYMax()
	{
		return _rangeYMax;
	}
	
	/**
	 * Gets the z max.
	 * @return the z max
	 */
	public int getZMax()
	{
		return _rangeZMax;
	}
	
	/**
	 * Sets the range.
	 * @param xMin the x min
	 * @param yMin the y min
	 * @param zMin the z min
	 * @param xMax the x max
	 * @param yMax the y max
	 * @param zMax the z max
	 */
	public void setRange(int xMin, int yMin, int zMin, int xMax, int yMax, int zMax)
	{
		_rangeXMin = xMin;
		_rangeYMin = yMin;
		_rangeZMin = zMin;
		
		_rangeXMax = xMax;
		_rangeYMax = yMax;
		_rangeZMax = zMax;
		
		_A = (_rangeYMax * (_rangeZMax - _rangeZMin)) + (_rangeYMin * (_rangeZMin - _rangeZMax));
		_B = (_rangeZMin * (_rangeXMax - _rangeXMin)) + (_rangeZMax * (_rangeXMin - _rangeXMax));
		_C = (_rangeXMin * (_rangeYMax - _rangeYMin)) + (_rangeXMin * (_rangeYMin - _rangeYMax));
		_D = -1 * ((_rangeXMin * ((_rangeYMax * _rangeZMax) - (_rangeYMin * _rangeZMax))) + (_rangeXMax * ((_rangeYMin * _rangeZMin) - (_rangeYMin * _rangeZMax))) + (_rangeXMin * ((_rangeYMin * _rangeZMax) - (_rangeYMax * _rangeZMin))));
	}
	
	/**
	 * Gets the a.
	 * @return the a
	 */
	public int getA()
	{
		return _A;
	}
	
	/**
	 * Gets the b.
	 * @return the b
	 */
	public int getB()
	{
		return _B;
	}
	
	/**
	 * Gets the c.
	 * @return the c
	 */
	public int getC()
	{
		return _C;
	}
	
	/**
	 * Gets the d.
	 * @return the d
	 */
	public int getD()
	{
		return _D;
	}
	
	/**
	 * Gets the map region.
	 * @return the map region
	 */
	public int getMapRegion()
	{
		return _mapRegion;
	}
	
	/**
	 * Sets the map region.
	 * @param region the new map region
	 */
	public void setMapRegion(int region)
	{
		_mapRegion = region;
	}
	
	/**
	 * Gets the known siege guards.
	 * @return the known siege guards
	 */
	public Collection<L2SiegeGuardInstance> getKnownSiegeGuards()
	{
		final List<L2SiegeGuardInstance> result = new ArrayList<>();
		
		for (L2Object obj : getKnownList().getKnownObjects().values())
		{
			if (obj instanceof L2SiegeGuardInstance)
			{
				result.add((L2SiegeGuardInstance) obj);
			}
		}
		
		return result;
	}
	
	/**
	 * Gets the known fort siege guards.
	 * @return the known fort siege guards
	 */
	public Collection<L2FortSiegeGuardInstance> getKnownFortSiegeGuards()
	{
		final List<L2FortSiegeGuardInstance> result = new ArrayList<>();
		
		final Collection<L2Object> objs = getKnownList().getKnownObjects().values();
		{
			for (L2Object obj : objs)
			{
				if (obj instanceof L2FortSiegeGuardInstance)
				{
					result.add((L2FortSiegeGuardInstance) obj);
				}
			}
		}
		return result;
	}
	
	@Override
	public void reduceCurrentHp(double damage, L2Character attacker, boolean awake)
	{
		if (isAutoAttackable(attacker) || ((attacker instanceof L2PcInstance) && ((L2PcInstance) attacker).isGM()))
		{
			super.reduceCurrentHp(damage, attacker, awake);
		}
		else
		{
			super.reduceCurrentHp(0, attacker, awake);
		}
	}
	
	@Override
	public boolean doDie(L2Character killer)
	{
		if (!super.doDie(killer))
		{
			return false;
		}
		
		final boolean isFort = ((getFort() != null) && (getFort().getFortId() > 0) && getFort().getSiege().getIsInProgress());
		final boolean isCastle = ((getCastle() != null) && (getCastle().getCastleId() > 0) && getCastle().getSiege().getIsInProgress());
		
		if (isFort || isCastle)
		{
			broadcastPacket(SystemMessage.sendString("The castle gate has been broken down."));
		}
		return true;
	}
}
